package be.bandWidthOfBrothers.astraSveneca.prescription.controllers;

import be.bandWidthOfBrothers.astraSveneca.prescription.entities.Prescription;
import be.bandWidthOfBrothers.astraSveneca.prescription.repositories.PrescriptionRepo;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.security.Principal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PrescriptionControllerTest {

    @Mock
    private UserRepo userRepo;
    @Mock
    private PrescriptionRepo prescriptionRepository;
    @InjectMocks
    private PrescriptionController prescriptionController;

    private MockMvc mockMvc;

    @BeforeEach
    public void init(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(prescriptionController).build();
    }

    @Test
    public void testIfContextLoads(){
        assertThat(prescriptionController).isNotNull();
    }

    @Test
    public void testGetMappingWithUserNameAsPathvariable() throws Exception {
        Customer customer = new Customer();
        Mockito.when(userRepo.findUsersByUserNameEquals(Mockito.any())).thenReturn(List.of(customer));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/prescriptions"))
                .andExpect(MockMvcResultMatchers.view().name("prescriptions/PrescriptionView"));

        Mockito.verify(userRepo).findPharmacy("userName");
        Mockito.verify(userRepo).findCustomer("userName");
        Mockito.verify(userRepo).findDoctor("userName");

    }
    @Test
    public void findPrescriptionByDoctorTest() throws Exception {
        Doctor doctor = Mockito.mock(Doctor.class);
        Prescription prescription = Mockito.mock(Prescription.class);

        Mockito.when(userRepo.findUsersByUserNameEquals(Mockito.any())).thenReturn(List.of(doctor));
        Mockito.when(userRepo.findDoctor(Mockito.any())).thenReturn(doctor);
        Mockito.when(prescription.getDoctor()).thenReturn(doctor);


        this.mockMvc.perform(MockMvcRequestBuilders.get("/prescriptions/userName"));

        Mockito.verify(prescriptionRepository).findPrescriptionByDoctor(doctor);
    }

}