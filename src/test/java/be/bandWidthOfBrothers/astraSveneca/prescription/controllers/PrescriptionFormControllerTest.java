package be.bandWidthOfBrothers.astraSveneca.prescription.controllers;


import be.bandWidthOfBrothers.astraSveneca.prescription.entities.Prescription;
import be.bandWidthOfBrothers.astraSveneca.prescription.repositories.PrescriptionRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PrescriptionFormControllerTest {


    @Mock
    private PrescriptionRepo prescriptionRepo;
    @InjectMocks
    private PrescriptionFormController prescriptionFormController;
    private MockMvc mockMvc;

    @BeforeEach
    public void init(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(prescriptionFormController).build();
    }

    @Test
    public void testIfContextLoads(){
        assertThat(prescriptionFormController).isNotNull();
    }

    @Test
    public void testGetMapping() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/prescriptionForm"))
                .andExpect(MockMvcResultMatchers.view().name("prescriptions/PrescriptionFormView"));
    }
    @Test
    public void testIfPrescriptionIsSend() throws Exception {
        Prescription prescription = Mockito.mock(Prescription.class);
        prescriptionRepo.save(prescription);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/prescriptionForm")
                .param("userName","TestUsername")
                .param("prescribedMedicine","1"));
        Mockito.verify(prescriptionRepo).save(prescription);


    }

}