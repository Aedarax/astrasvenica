package be.bandWidthOfBrothers.astraSveneca.profile.controllers;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.medication.repository.MedicationRepo;
import be.bandWidthOfBrothers.astraSveneca.supplies.entities.Supply;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class ProfileControllerTest {

    @Mock
    private UserRepo userRepo;
    @Mock
    private MedicationRepo medicationRepo;
    @InjectMocks
    private ProfileController profileController;

    private MockMvc mockMvc;

    @BeforeEach
    public void init(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.profileController).build();
    }

    @Test
    public void testIfContextLoads(){
        assertThat(profileController).isNotNull();
    }

    @Test
    public void testGetMappingWithUsernameAsPathvariable() throws Exception {
        Customer customer = new Customer();
        Mockito.when(userRepo.findUsersByUserNameEquals(Mockito.any())).thenReturn(List.of(customer));

       this.mockMvc.perform(MockMvcRequestBuilders.get("/profile/userName"))
               .andExpect(MockMvcResultMatchers.view().name("profile/ProfileView"));
        Mockito.verify(userRepo).findCustomer("userName");
        Mockito.verify(userRepo).findDoctor("userName");
        Mockito.verify(userRepo).findPharmacy("userName");
    }

    @Test
    public void testIfPharmacyHasAList() throws Exception {
        Supply supply = Mockito.mock(Supply.class);
        Pharmacy pharmacy = Mockito.mock(Pharmacy.class);
        Medication medication = Mockito.mock(Medication.class);

        Mockito.when(userRepo.findUsersByUserNameEquals(Mockito.any())).thenReturn(List.of(pharmacy));
        Mockito.when(userRepo.findPharmacy(Mockito.any())).thenReturn(pharmacy);
        Mockito.when(supply.getMedication()).thenReturn(medication);
        Mockito.when(medication.getId()).thenReturn(1);
        Mockito.when(pharmacy.getSupplySet()).thenReturn(Set.of(supply));

        this.mockMvc.perform(MockMvcRequestBuilders.get("/profile/userName"));

        Mockito.verify(medicationRepo).findMedicationById(1);

    }
}