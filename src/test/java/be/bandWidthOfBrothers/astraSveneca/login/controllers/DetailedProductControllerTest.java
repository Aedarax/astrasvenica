package be.bandWidthOfBrothers.astraSveneca.login.controllers;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.medication.repository.MedicationRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@WebAppConfiguration
class DetailedProductControllerTest {
    @Autowired
    private MedicationRepo medicationRepo;
    @Autowired
    private DetailedProductController detailedProductController;
    private MockMvc mockMvc;

    @BeforeEach
    public void init(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.detailedProductController).build();
    }

    @Test
    public void testIfContextLoads(){
        assertThat(detailedProductController).isNotNull();
    }

    @Test
    public void testRequestMapping() throws Exception {
        Medication medication = new Medication("TestName","TestDescription",null, false);
        medication.setId(1);
        medicationRepo.save(medication);
        mockMvc.perform(MockMvcRequestBuilders.get("/productDetail").param("medicationId","1"))
                .andExpect(MockMvcResultMatchers.view().name("productDetail/ProductDetailView" ));


    }


}