package be.bandWidthOfBrothers.astraSveneca.registry.controllers;

import be.bandWidthOfBrothers.astraSveneca.users.commands.UserCommand;
import be.bandWidthOfBrothers.astraSveneca.users.converters.UserCommandConverter;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PharmacyRegistrationControllerTest {

    @Mock
    private UserRepo userRepo;
    @Mock
    private UserCommandConverter userCommandConverter;
    @InjectMocks
    private PharmacyRegistrationController pharmacyRegistrationController;

    private MockMvc mockMvc;


    @BeforeEach
    public void init(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.pharmacyRegistrationController).build();
    }

    @Test
    public void testIfContextLoads(){
        assertThat(pharmacyRegistrationController).isNotNull();
    }

    @Test
    public void testGetMapping() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/register/pharmacy"))
                .andExpect(MockMvcResultMatchers.view().name("register/pharmacy/PharmacyRegistrationView"));
    }

    @Test
    public void testIfRegistrationIsSuccessful() throws Exception {
        UserCommand userCommand = new UserCommand("TestFirstName", "TestLastName", "TestName", "TestUserName", "Password1", "Password1");
        userCommand.setStreetName("TestStreetName");
        Pharmacy pharmacy = new Pharmacy("TestPharmacy","TestStreet","6","9898","TestCity","TestCountry");
        pharmacy.setUserName("TestUsername");
        pharmacy.setPassword("Password1");
        pharmacy.setPasswordbc("Password1");
        pharmacy.setRole("ROLE_ROLE");
        Mockito.when(userCommandConverter.convertPharmacy(userCommand)).thenReturn(pharmacy);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/register/pharmacy").flashAttr("userCommand",userCommand))
                .andExpect(MockMvcResultMatchers.view().name("redirect:/login"));
        Mockito.verify(userCommandConverter).convertPharmacy(userCommand);
        Mockito.verify(userRepo).save(pharmacy);




    }

}