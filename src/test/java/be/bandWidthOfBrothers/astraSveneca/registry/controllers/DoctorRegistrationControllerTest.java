package be.bandWidthOfBrothers.astraSveneca.registry.controllers;

import be.bandWidthOfBrothers.astraSveneca.users.commands.UserCommand;
import be.bandWidthOfBrothers.astraSveneca.users.converters.UserCommandConverter;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.Assertions.assertThat;



@ExtendWith(MockitoExtension.class)
class DoctorRegistrationControllerTest {

    @Mock
    private UserRepo userRepo;
    @Mock
    private UserCommandConverter userCommandConverter;
    @InjectMocks
    private DoctorRegistrationController doctorRegistrationController;

    private MockMvc mockMvc;


    @BeforeEach
    public void init() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.doctorRegistrationController).build();
    }

    @Test
    public void testIfContextLoads() {
        assertThat(doctorRegistrationController).isNotNull();
    }

    @Test
    public void testGetMapping() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/register/doctor"))
                .andExpect(MockMvcResultMatchers.view().name("register/doctor/DoctorRegistrationView"));
    }

    @Test
    public void testIfRegistrationIsSuccessful() throws Exception {
        UserCommand userCommand = new UserCommand("TestFirstName", "TestLastName", "TestName", "TestUserName", "Password1", "Password1");
        userCommand.setStreetName("TestStreetName");
        Doctor doctor = new Doctor("TestDoctorFirstName", "TestDoctorLastName");
        doctor.setUserName("TestDoctorUserName");
        doctor.setPassword("Password1");
        doctor.setPasswordbc("Password1");
        doctor.setRole("ROLE_ROLE");
        Mockito.when(userCommandConverter.convertDoctor(userCommand)).thenReturn(doctor);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/register/doctor").flashAttr("userCommand", userCommand))
                .andExpect(MockMvcResultMatchers.view().name("redirect:/login"));
        Mockito.verify(userCommandConverter).convertDoctor(userCommand);
        Mockito.verify(userRepo).save(doctor);

    }
}