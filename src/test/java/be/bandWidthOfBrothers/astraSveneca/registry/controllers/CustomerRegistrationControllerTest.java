package be.bandWidthOfBrothers.astraSveneca.registry.controllers;

import be.bandWidthOfBrothers.astraSveneca.users.commands.UserCommand;
import be.bandWidthOfBrothers.astraSveneca.users.converters.UserCommandConverter;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CustomerRegistrationControllerTest {

    @Mock
    private UserRepo userRepo;
    @Mock
    private UserCommandConverter userCommandConverter;
    @InjectMocks
    private CustomerRegistrationController customerRegistrationController;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.customerRegistrationController).build();

    }

    @Test
    public void testIfContextLoads() {
        assertThat(customerRegistrationController).isNotNull();
    }

    @Test
    public void testGetMapping() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/register/customer"))
                .andExpect(MockMvcResultMatchers.view().name("register/customer/CustomerRegistrationView"));
    }

    @Test
    public void testIfRegistrationIsSuccessful() throws Exception {
        UserCommand userCommand = new UserCommand("TestFirstName", "TestLastName", "TestName", "TestUserName", "Password1", "Password1");
        userCommand.setStreetName("TestStreetName");
        Customer customer = new Customer("TestFirstName", "TestLastName", "TestStreetName", "6", "9900", "TestCity", "TestCountry");
        customer.setPassword("Password");
        customer.setPasswordbc("Password");
        customer.setUserName("UName");
        customer.setRole("ROLE_ROLE");
        Mockito.when(userCommandConverter.convertCustomer(userCommand)).thenReturn(customer);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/register/customer").flashAttr("userCommand", userCommand))
                .andExpect(MockMvcResultMatchers.view().name("redirect:/login"));
        Mockito.verify(userCommandConverter).convertCustomer(userCommand);
        Mockito.verify(userRepo).save(customer);

    }
}