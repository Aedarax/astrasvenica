insert into Customers values
(1,'Testcity','Testcountry',true,'6','Password1','Password1','customer','Teststreet','Testcustomer','9900','Pietje','Pol');
-- insert into Doctors values
-- (2,'Password1','Password1',1,'doctor','Testdoctor','Pol','Pietje');
insert into Pharmacies values
(3,'Testcity','Testcountry',true,'66','Password1','Password1','pharmacy','Teststreet','Testpharmacy','9900','Pharmacyname');
insert into Medication values
(1,'Testdescription',null,'Testmedication');
insert into Orders values
(1,1,3);
insert into Orders_Medication values
(1,1);
-- insert into Prescriptions values
-- (1,0,1,2);
-- insert into Prescriptions_Medication values
-- (1,1);
insert into Supplies values
(1,5,100,1,3);
