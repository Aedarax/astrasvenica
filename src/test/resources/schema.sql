create table if not exists Customers(
    Id integer identity not null,
    City varchar(255),
    Country varchar(255),
    Enabled boolean,
    HouseNumber varchar(255),
    Password varchar(255),
    Passwordbc varchar(255),
    Role varchar(255),
    StreetName varchar(255),
    UserName varchar(255),
    Zipcode varchar(255),
    FirstName varchar(255),
    LastName varchar(255),
    PRIMARY key(Id)
);

create table if not exists Doctors(
    Id integer,
    Password varchar(255),
    Passwordbc varchar(255),
    Enabled boolean,
    Role varchar(255),
    UserName varchar(255),
    FirstName varchar(255),
    LastName varchar(255),
    PRIMARY key(Id)
);

create table if not exists Pharmacies(
    Id integer,
    City varchar(300),
    Country varchar(255),
    Enabled boolean,
    HouseNumber varchar(255),
    Password varchar(255),
    Passwordbc varchar(255),
    Role varchar(255),
    StreetName varchar(255),
    UserName varchar(255),
    Zipcode varchar(255),
    Name varchar(255),
    PRIMARY key(Id)
);

create table if not exists Medication(
    Id integer identity not null,
    Description varchar(255),
    Image varchar(255),
    Name varchar(255),
    PRIMARY key(Id)
);

create table if not exists Orders(
    Id integer,

    Customer_id integer,
    Pharmacy_id integer,
    foreign key (Customer_id) references Customers(Id) ,
    foreign key (Pharmacy_id) references Pharmacies(Id),
    PRIMARY key(Id)
);

create table if not exists Orders_Medication(
    Order_id integer,
    Medication_id integer,
    foreign key (Order_id)references Orders(Id),
    foreign key (Medication_id)references Medication(Id)
);



create table if not exists Prescriptions(
    Id integer identity not null,
    Used boolean,
    Customer_Id integer,
    Doctor_Id integer,
    foreign key (Customer_Id) references Customers(Id),
    foreign key (Doctor_id) references Doctors(Id),
    PRIMARY key(Id)
);

create table if not exists Prescriptions_Medication(
    Prescription_Id integer,
    Medication_Id integer,
    foreign key (Prescription_Id) references Prescriptions(Id),
    foreign key (Medication_Id) references Medication(Id)
);

create table if not exists Supplies(
    Id integer identity  not null,
    Price integer,
    Stock integer,
    Medication_Id integer,
    Pharmacy_Id integer,
    foreign key (Medication_Id) references Medication(Id),
    foreign key (Pharmacy_Id) references Pharmacies(Id),
    PRIMARY key(Id)
);

