document.getElementById("date").style.display = "none";

window.onscroll = function () {stickyCart()};

var cart = document.getElementById("cart-wrapper");
var stickyCartTop = cart.offsetTop;

function showDate(){
    document.getElementById("date").style.display = "inline";
}

function hideDate(){
    document.getElementById("date").style.display = "none";
}

function checkPrescription(boolean){
    if (!boolean){
        alert("Voorschrift nodig")
    }
}

function stickyCart() {
    if (window.pageYOffset > stickyCartTop) {
        cart.classList.add("sticky");
    } else {
        cart.classList.remove("sticky");
    }
}
