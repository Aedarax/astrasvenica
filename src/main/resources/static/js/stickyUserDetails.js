window.onscroll = function() {stickyUserDetails()};

var userWrapper = document.getElementById("user-wrapper");
var sticky = userWrapper.offsetTop;

function stickyUserDetails() {
    if (window.pageYOffset > sticky) {
        userWrapper.classList.add("sticky");
    } else {
        userWrapper.classList.remove("sticky");
    }
}