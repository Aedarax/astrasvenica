package be.bandWidthOfBrothers.astraSveneca.exceptions;

public class UsernameDoesntExistException extends RuntimeException {

    public UsernameDoesntExistException() {
    }

    public UsernameDoesntExistException(String message) {
        super(message);
    }

    public UsernameDoesntExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public UsernameDoesntExistException(Throwable cause) {
        super(cause);
    }
}
