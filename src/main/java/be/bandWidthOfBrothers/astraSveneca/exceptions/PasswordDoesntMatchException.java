package be.bandWidthOfBrothers.astraSveneca.exceptions;

public class PasswordDoesntMatchException extends RuntimeException{
    public PasswordDoesntMatchException() {
    }

    public PasswordDoesntMatchException(String message) {
        super(message);
    }

    public PasswordDoesntMatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public PasswordDoesntMatchException(Throwable cause) {
        super(cause);
    }
}
