package be.bandWidthOfBrothers.astraSveneca.registry.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("register")
public class GeneralRegistrationController {


    @PostMapping
    public String redirect(@RequestParam("user") String user) {
        switch (user) {
            case "Klant":
                return "redirect:register/customer";
            case "Apotheker":
                return "redirect:register/pharmacy";
            case "Dokter":
                return "redirect:register/doctor";
            default:
                return "redirect:/login";
        }
    }

}
