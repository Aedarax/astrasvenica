package be.bandWidthOfBrothers.astraSveneca.registry.controllers;

import be.bandWidthOfBrothers.astraSveneca.exceptions.PasswordDoesntMatchException;
import be.bandWidthOfBrothers.astraSveneca.exceptions.UsernameAlreadyExistsException;
import be.bandWidthOfBrothers.astraSveneca.users.commands.UserCommand;
import be.bandWidthOfBrothers.astraSveneca.users.converters.UserCommandConverter;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.validation.Valid;

@Controller
@RequestMapping("register/doctor")
public class DoctorRegistrationController {
    public static final String FORM_VIEW = "register/doctor/DoctorRegistrationView";
    private UserCommandConverter commandConverter;
    private UserRepo userRepo;
    private UserCommand user = new UserCommand();

    public DoctorRegistrationController(UserCommandConverter commandConverter, UserRepo userRepo) {
        this.commandConverter = commandConverter;
        this.userRepo = userRepo;
    }

    @GetMapping
    public String showDoctorRegistration(Model model) {
        model.addAttribute("user", user);
        return FORM_VIEW;
    }

    @PostMapping
    public String registrationSuccessful(@Valid UserCommand userCommand, BindingResult bindingResult, Model model) {
        if(bindingResult.hasFieldErrors()) {
            model.addAttribute("user", userCommand);
            return FORM_VIEW;
        } else {
            try {
                Doctor doctor = commandConverter.convertDoctor(userCommand);
                userRepo.save(doctor);
                return "redirect:/login";
            } catch (PasswordDoesntMatchException pdme) {
                model.addAttribute("user", userCommand);
                model.addAttribute("passwordMessage", "Wachtwoorden komen niet overeen.");
                return FORM_VIEW;
            } catch (UsernameAlreadyExistsException uaee) {
                model.addAttribute("user", userCommand);
                model.addAttribute("usernameMessage", "Gebruikersnaam al in gebruik.");
                return FORM_VIEW;
            }
        }
    }
}
