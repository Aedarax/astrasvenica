package be.bandWidthOfBrothers.astraSveneca.prescription.entities;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name ="Prescriptions")
public class Prescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "Used")
    private boolean used;
    @ManyToOne
    private Customer customer;
    @ManyToOne
    private Doctor doctor;
    @ManyToMany
    private List<Medication> medication = new ArrayList<>();

    public Prescription() {
    }

    public Prescription(boolean used, Customer customer, Doctor doctor, List<Medication> medication) {
        this.used = used;
        this.customer = customer;
        this.doctor = doctor;
        this.medication = medication;
    }

    public int getId() {
        return id;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public List<Medication> getMedication() {
        return medication;
    }

    public void setMedication(List<Medication> medication) {
        this.medication = medication;
    }
}
