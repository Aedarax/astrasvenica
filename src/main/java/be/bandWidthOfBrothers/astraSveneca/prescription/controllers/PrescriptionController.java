package be.bandWidthOfBrothers.astraSveneca.prescription.controllers;

import be.bandWidthOfBrothers.astraSveneca.medication.repository.MedicationRepo;
import be.bandWidthOfBrothers.astraSveneca.prescription.entities.Prescription;
import be.bandWidthOfBrothers.astraSveneca.prescription.repositories.PrescriptionRepo;
import be.bandWidthOfBrothers.astraSveneca.users.User;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("prescriptions")
public class PrescriptionController {
    private UserRepo userRepo;
    private PrescriptionRepo prescriptionRepo;


    public PrescriptionController(UserRepo userRepo, PrescriptionRepo prescriptionRepo) {
        this.userRepo = userRepo;
        this.prescriptionRepo = prescriptionRepo;

    }

    @GetMapping
    public String showPrescriptions(Model model, Principal principal) {
        Customer customer = userRepo.findCustomer(principal.getName());
        Pharmacy pharmacy = userRepo.findPharmacy(principal.getName());
        Doctor doctor = userRepo.findDoctor(principal.getName());
        User user = userRepo.findUsersByUserNameEquals(principal.getName()).get(0);
        if(doctor != null) {
            Set<Prescription> prescriptions = prescriptionRepo.findPrescriptionByDoctor(doctor);
            model.addAttribute("prescriptions", prescriptions);
        }
        if(customer != null) {
            List<Prescription>  prescriptions = prescriptionRepo.findPrescriptionByCustomer(customer);
            model.addAttribute("prescriptions", prescriptions);
        }
        model.addAttribute("user", user);
        model.addAttribute("customer", customer);
        model.addAttribute("pharmacy", pharmacy);
        model.addAttribute("doctor", doctor);
        return "prescriptions/PrescriptionView";
    }

}
