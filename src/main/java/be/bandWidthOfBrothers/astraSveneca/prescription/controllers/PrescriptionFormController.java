package be.bandWidthOfBrothers.astraSveneca.prescription.controllers;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.medication.repository.MedicationRepo;
import be.bandWidthOfBrothers.astraSveneca.prescription.entities.Prescription;
import be.bandWidthOfBrothers.astraSveneca.prescription.repositories.PrescriptionRepo;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("prescriptionForm")
public class PrescriptionFormController {

    private UserRepo userRepo;
    private MedicationRepo medicationRepo;
    private PrescriptionRepo prescriptionRepo;

    public PrescriptionFormController(UserRepo userRepo, MedicationRepo medicationRepo, PrescriptionRepo prescriptionRepo) {
        this.userRepo = userRepo;
        this.medicationRepo = medicationRepo;
        this.prescriptionRepo = prescriptionRepo;
    }

    @GetMapping
    public String prescriptionForm(){
        return "prescriptions/PrescriptionFormView";
    }

    @PostMapping
    public String sendingPrescriptionForm(@RequestParam("userName") String userName,
                                          @RequestParam("prescribedMedicine") Medication medication,
                                          Principal principal){
        Customer customer = userRepo.findCustomer(userName);
        Doctor doctor = userRepo.findDoctor(principal.getName());
        Prescription prescription = new Prescription(false,customer,doctor,List.of(medication));
        prescriptionRepo.save(prescription);
        return "prescriptions/PrescriptionView";

    }
}
