package be.bandWidthOfBrothers.astraSveneca.prescription.repositories;

import be.bandWidthOfBrothers.astraSveneca.prescription.entities.Prescription;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Set;

@Repository
public interface PrescriptionRepo extends JpaRepository<Prescription, Integer> {

    List<Prescription> findPrescriptionByCustomer(Customer customer);
    Set<Prescription> findPrescriptionByDoctor(Doctor doctor);
}
