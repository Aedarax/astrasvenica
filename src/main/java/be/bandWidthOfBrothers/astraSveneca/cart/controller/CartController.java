package be.bandWidthOfBrothers.astraSveneca.cart.controller;

import be.bandWidthOfBrothers.astraSveneca.cart.entities.Cart;
import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.orders.entities.Order;
import be.bandWidthOfBrothers.astraSveneca.orders.entities.SoldItem;
import be.bandWidthOfBrothers.astraSveneca.orders.repositories.OrderRepo;
import be.bandWidthOfBrothers.astraSveneca.orders.repositories.SoldItemRepo;
import be.bandWidthOfBrothers.astraSveneca.prescription.entities.Prescription;
import be.bandWidthOfBrothers.astraSveneca.prescription.repositories.PrescriptionRepo;
import be.bandWidthOfBrothers.astraSveneca.supplies.entities.Supply;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("cart")
public class CartController {

    private UserRepo userRepo;
    private SoldItemRepo soldItemRepo;
    private OrderRepo orderRepo;
    private PrescriptionRepo prescriptionRepo;
    private Cart cart;
    private Cart userCart;
    boolean createOrder = true;

    public CartController(UserRepo userRepo,SoldItemRepo soldItemRepo ,Cart cart, Cart userCart, OrderRepo orderRepo, PrescriptionRepo prescriptionRepo) {
        this.userRepo = userRepo;
        this.soldItemRepo = soldItemRepo;
        this.cart = cart;
        this.userCart = userCart;
        this.orderRepo = orderRepo;
        this.prescriptionRepo = prescriptionRepo;
    }

    @GetMapping()
    public String showCart(Model model, Principal principal){
        model.addAttribute("isOrderCreated", createOrder);
        model.addAttribute("localDate", LocalDate.now().plusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE));
        if (principal  != null){
            userCart = userRepo.findUsersByUserNameEquals(principal.getName()).get(0).getCart();
            cart.getSupplies().forEach(a -> userCart.getSupplies().add(a));
            model.addAttribute("cart", userCart.getSupplies());
        } else {
            model.addAttribute("cart", cart.getSupplies());
        }
        return "cart/CartView";
    }

    @PostMapping
    public String orderCart(Principal principal,
                            @RequestParam(value = "delivery") String delivery,
                            @RequestParam(value = "remark", required = false) String remark,
                            @RequestParam(value = "pickupDate", required = false)@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate date,
                            HttpServletRequest request,
                            Model model){
        if (principal == null){
            return "redirect:login";
        }
        createOrder = true;
        Order order = new Order(userRepo.findCustomer(principal.getName()));
        String[] amountArray = request.getParameterMap().get("amount");
        String[] selectArray = request.getParameterMap().get("selection");
        List<Supply> suppliesToBuy = new ArrayList<>();
        List<SoldItem> itemsToSell = new ArrayList<>();
        for (int i = 0 ; i < amountArray.length ; i++){
            userCart.getSupplies().get(i).setAmount(Integer.parseInt(amountArray[i]));
        }
        if (selectArray == null){
            return "redirect:cart";
        }
        for (String s:selectArray) {
            suppliesToBuy.add(userCart.getSupplies().get(Integer.parseInt(s)));
        }
        supplyPrescriptionCheck(principal, delivery, remark, date, order, suppliesToBuy, itemsToSell);
        model.addAttribute("isOrderCreated", createOrder);
        if (createOrder){
            orderRepo.save(order);
            soldItemRepo.saveAll(itemsToSell);
            userCart.getSupplies().clear();
            cart.getSupplies().clear();
        } else {
            createOrder = true;
            return "redirect:cart";
        }
        return "redirect:orders";
    }

    private void supplyPrescriptionCheck(Principal principal, String delivery, String remark, LocalDate date, Order order, List<Supply> suppliesToBuy, List<SoldItem> itemsToSell) {
        for (Supply s: suppliesToBuy) {
            if (!prescriptionCheck(principal, s.getMedication())){
                deliveryCheck(delivery, remark, date, order, itemsToSell, s);
            } else if (s.getMedication().isPrescriptionRequired()){
                createOrder = false;
            } else if (!s.getMedication().isPrescriptionRequired()){
                deliveryCheck(delivery, remark, date, order, itemsToSell, s);
            }
        }
    }

    private void deliveryCheck(String delivery, String remark, LocalDate date, Order order, List<SoldItem> itemsToSell, Supply s) {
        SoldItem soldItem;
        if (delivery.equals("leveren")){
            soldItem = new SoldItem(order, s.getMedication(), s.getPharmacy(), s.getAmount(), s.getPrice() * 0.5, remark);
            soldItem.setDelivery(true);
        } else {
            soldItem = new SoldItem(order, s.getMedication(), s.getPharmacy(), s.getAmount(), s.getPrice() * 0.5, remark, date);
            soldItem.setDelivery(false);
        }
        s.setStock(s.getStock() - s.getAmount());
        itemsToSell.add(soldItem);
    }

    private boolean prescriptionCheck(Principal principal, Medication medication){
        List<Prescription> prescriptionList = prescriptionRepo.findPrescriptionByCustomer(userRepo.findCustomer(principal.getName()));
        List<Medication> prescribedMedication = new ArrayList<>();
        for (Prescription p : prescriptionList){
            for (Medication m: p.getMedication()){
                if (m.getId() == medication.getId()){
                    prescribedMedication.add(m);
                }
            }
        } return prescribedMedication.isEmpty();
    }
}