package be.bandWidthOfBrothers.astraSveneca.users.pharmacy;

import be.bandWidthOfBrothers.astraSveneca.supplies.entities.Supply;
import be.bandWidthOfBrothers.astraSveneca.users.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

/**
 * Behlül, Nihad, Robin
 */
@Entity
@Table(name = "Pharmacies")
public class Pharmacy extends User {

    @NotBlank
    @Column(name = "Name")
    private String name;
    @OneToMany(mappedBy = "pharmacy")
    private Set<Supply> supplySet = new HashSet<>();


    public Pharmacy() {
        setRole("pharmacy");
    }

    public Pharmacy(String name, String streetName, String houseNumber, String zipCode, String city, String country) {
        this.name = name;
        setStreetName(streetName);
        setHouseNumber(houseNumber);
        setZipCode(zipCode);
        setCity(city);
        setCountry(country);
        setRole("pharmacy");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Supply> getSupplySet() {
        return supplySet;
    }

    public void setSupplySet(Set<Supply> supplySet) {
        this.supplySet = supplySet;
    }

    @Override
    public String toString() {
        return "Pharmacy{" +
                "name='" + name + '\'' +
                ", supplySet=" + supplySet +
                "} " + super.toString();
    }
}
