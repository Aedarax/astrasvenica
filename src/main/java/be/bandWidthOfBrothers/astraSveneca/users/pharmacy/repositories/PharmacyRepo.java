package be.bandWidthOfBrothers.astraSveneca.users.pharmacy.repositories;

import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PharmacyRepo extends JpaRepository<Pharmacy,Integer> {


}
