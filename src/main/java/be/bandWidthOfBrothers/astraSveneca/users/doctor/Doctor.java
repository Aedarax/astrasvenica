package be.bandWidthOfBrothers.astraSveneca.users.doctor;

import be.bandWidthOfBrothers.astraSveneca.users.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Behlül, Nihad, Robin
 */
@Entity
@Table(name="Doctors")
public class Doctor extends User {


    @NotBlank
    @Column(name = "FirstName")
    private String firstName;
    @NotBlank
    @Column(name = "LastName")
    private String lastName;


    public Doctor() {
        setRole("doctor");
    }

    public Doctor(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        setRole("doctor");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                "} " + super.toString();
    }
}
