package be.bandWidthOfBrothers.astraSveneca.users.repositories;

import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import be.bandWidthOfBrothers.astraSveneca.users.User;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {

    List<User> findUsersByUserNameEquals(String userName);

    @Query("SELECT c from Customer c where c.userName=?1")
    Customer findCustomer(String userName);

    @Query("SELECT c from Doctor c where c.userName=?1")
    Doctor findDoctor(String userName);

    @Query("SELECT c from Pharmacy c where c.userName=?1")
    Pharmacy findPharmacy(String userName);
}
