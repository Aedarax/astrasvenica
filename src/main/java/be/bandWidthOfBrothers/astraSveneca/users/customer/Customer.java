package be.bandWidthOfBrothers.astraSveneca.users.customer;

import be.bandWidthOfBrothers.astraSveneca.users.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Behlül, Nihad, Robin
 */
@Entity
@Table(name = "Customers")
public class Customer extends User {

    @NotBlank
    @Column(name = "FirstName")
    private String firstName;
    @NotBlank
    @Column(name = "LastName")
    private String lastName;

    public Customer() {
        setRole("Customer");
    }

    public Customer(String firstName, String lastName, String streetName, String houseNumber, String zipCode, String city, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        setStreetName(streetName);
        setHouseNumber(houseNumber);
        setZipCode(zipCode);
        setCity(city);
        setCountry(country);
        setRole("Customer");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                "} " + super.toString();
    }
}
