package be.bandWidthOfBrothers.astraSveneca.users.converters;

import be.bandWidthOfBrothers.astraSveneca.exceptions.PasswordDoesntMatchException;
import be.bandWidthOfBrothers.astraSveneca.exceptions.UsernameAlreadyExistsException;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import be.bandWidthOfBrothers.astraSveneca.users.commands.UserCommand;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserCommandConverter {

    private PasswordEncoder passwordEncoder;

    private UserRepo userRepo;

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public UserCommandConverter() {
    }

    public PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public Customer convertCustomer(UserCommand userCommand) {
        if(checkUsername(userCommand)) {
            if (userCommand.getPassword().equals(userCommand.getConfirmPassword())) {
                String encodedPassword = passwordEncoder.encode(userCommand.getPassword());
                Customer customer = new Customer(userCommand.getFirstName(), userCommand.getLastName(),
                        userCommand.getStreetName(), userCommand.getHouseNumber(),
                        userCommand.getZipCode(), userCommand.getCity(), userCommand.getCountry());
                customer.setUserName(userCommand.getUserName());
                customer.setPassword(encodedPassword);
                customer.setPasswordbc(encodedPassword);
                customer.setRole("customer");
                return customer;
            } else {
                throw new PasswordDoesntMatchException();
            }
        } else {
            throw new UsernameAlreadyExistsException();
        }
    }

    public Doctor convertDoctor(UserCommand userCommand) {
        if(checkUsername(userCommand)) {
            if (userCommand.getPassword().equals(userCommand.getConfirmPassword())) {
                String encodedPassword = passwordEncoder.encode(userCommand.getPassword());
                Doctor doctor = new Doctor(userCommand.getFirstName(), userCommand.getLastName());
                doctor.setUserName(userCommand.getUserName());
                doctor.setPassword(encodedPassword);
                doctor.setPasswordbc(encodedPassword);
                doctor.setRole("doctor");
                return doctor;
            } else {
                throw new PasswordDoesntMatchException();
            }
        } else {
            throw new UsernameAlreadyExistsException();
        }
    }

    public Pharmacy convertPharmacy(UserCommand userCommand) {
        if(checkUsername(userCommand)) {
            if (userCommand.getPassword().equals(userCommand.getConfirmPassword())) {
                String encodedPassword = passwordEncoder.encode(userCommand.getPassword());
                Pharmacy pharmacy = new Pharmacy(userCommand.getName(), userCommand.getStreetName(),
                        userCommand.getHouseNumber(),
                        userCommand.getZipCode(), userCommand.getCity(), userCommand.getCountry());
                pharmacy.setUserName(userCommand.getUserName());
                pharmacy.setPassword(encodedPassword);
                pharmacy.setPasswordbc(encodedPassword);
                pharmacy.setRole("pharmacy");
                return pharmacy;
            } else {
                throw new PasswordDoesntMatchException();
            }
        } else {
            throw new UsernameAlreadyExistsException();
        }
    }

    private boolean checkUsername(UserCommand userCommand) {
        return userRepo.findUsersByUserNameEquals(userCommand.getUserName()).isEmpty();
    }
}

