package be.bandWidthOfBrothers.astraSveneca.users;

import be.bandWidthOfBrothers.astraSveneca.cart.entities.Cart;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
/**
 * Behlül, Nihad, Robin
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class User {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "Id")
    private int id;
    @NotBlank
    @Size(min = 4)
    @Column(name = "UserName")
    private String userName;
    @Column(name = "Password")
    private String password;
    @NotBlank
    @Size(min = 3)
    private String passwordbc;

    private boolean enabled;
    private String role;

    @Column(name = "StreetName")
    private String streetName;
    @Column(name = "HouseNumber")
    private String houseNumber;
    @Column(name = "ZipCode")
    private String zipCode;
    @Column(name = "City")
    private String city;
    @Column(name = "Country")
    private String country;
    @Transient
    private Cart cart = new Cart();

    public User() {
        this.enabled = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordbc() {
        return passwordbc;
    }

    public void setPasswordbc(String passwordbc) {
        this.passwordbc = passwordbc;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", role='" + role + '\'' +
                ", streetName='" + streetName + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", cart=" + cart +
                '}';
    }
}
