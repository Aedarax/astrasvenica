package be.bandWidthOfBrothers.astraSveneca.login.controllers;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.medication.repository.MedicationRepo;
import be.bandWidthOfBrothers.astraSveneca.cart.entities.Cart;
import be.bandWidthOfBrothers.astraSveneca.prescription.repositories.PrescriptionRepo;
import be.bandWidthOfBrothers.astraSveneca.supplies.entities.Supply;
import be.bandWidthOfBrothers.astraSveneca.supplies.repository.SupplyRepo;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
@RequestMapping("productDetail")
public class DetailedProductController {

    private MedicationRepo medicationRepo;
    private SupplyRepo supplyRepository;
    private UserRepo userRepo;
    private Cart cart;

    @Autowired
    public DetailedProductController(MedicationRepo medicationRepo, SupplyRepo supplyRepository, Cart cart, UserRepo userRepo) {
        this.medicationRepo = medicationRepo;
        this.supplyRepository = supplyRepository;
        this.userRepo = userRepo;
        this.cart = cart;
    }

    @GetMapping
    public String detailedProduct(@RequestParam ("medicationId") int id, Model model){
        Medication medication = medicationRepo.findMedicationById(id);
        model.addAttribute("product", medication);
        model.addAttribute("suppliers", medication.getSupplySet());
        return "productDetail/ProductDetailView";
    }

    @PostMapping
    public String addProductToCart(@RequestParam("supplyId") int supplyId){
        Supply supply = supplyRepository.findById(supplyId).get();
        cart.getSupplies().add(supply);
        return "redirect:productDetail?medicationId=" + supply.getMedication().getId();
    }

}
