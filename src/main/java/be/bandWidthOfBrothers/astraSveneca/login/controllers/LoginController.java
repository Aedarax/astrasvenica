package be.bandWidthOfBrothers.astraSveneca.login.controllers;

import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    private PasswordEncoder passwordEncoder;
    private UserRepo userRepo;

    @Autowired
    public LoginController(UserRepo userRepo, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public String showLoginPage(){
        return "login/LoginView";
    }

}
