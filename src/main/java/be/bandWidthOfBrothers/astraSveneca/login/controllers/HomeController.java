package be.bandWidthOfBrothers.astraSveneca.login.controllers;

import be.bandWidthOfBrothers.astraSveneca.medication.repository.MedicationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {

    private MedicationRepo medicationRepo;

    @Autowired
    public HomeController(MedicationRepo medicationRepo) {
        this.medicationRepo = medicationRepo;
    }

    @GetMapping
    public String showHome(Model model){
        model.addAttribute("medicationList", medicationRepo.findAll());
        return "home/HomeView";
    }
}
