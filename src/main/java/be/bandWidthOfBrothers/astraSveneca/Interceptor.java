package be.bandWidthOfBrothers.astraSveneca;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.medication.repository.MedicationRepo;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class Interceptor implements HandlerInterceptor {


    private MedicationRepo medicationRepo;


    public Interceptor(MedicationRepo medicationRepo) {
        this.medicationRepo = medicationRepo;

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        List<Medication> medicationList = medicationRepo.findAll();
        if(modelAndView != null){
            modelAndView.addObject("medicationList",medicationList);
        }

    }
}
