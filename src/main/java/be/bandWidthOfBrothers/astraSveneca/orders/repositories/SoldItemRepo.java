package be.bandWidthOfBrothers.astraSveneca.orders.repositories;

import be.bandWidthOfBrothers.astraSveneca.orders.entities.Order;
import be.bandWidthOfBrothers.astraSveneca.orders.entities.SoldItem;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SoldItemRepo extends JpaRepository<SoldItem,Integer> {

    List<SoldItem> findSoldItemsByOrder(Order order);
    List<SoldItem> findSoldItemsByPharmacyOrderByOrderLocalDateTimeDesc(Pharmacy pharmacy);
}
