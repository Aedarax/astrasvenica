package be.bandWidthOfBrothers.astraSveneca.orders.repositories;

import be.bandWidthOfBrothers.astraSveneca.orders.entities.Order;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepo extends JpaRepository<Order, Integer> {

    List<Order> findOrdersByCustomerOrderByIdDesc(Customer customer);
}
