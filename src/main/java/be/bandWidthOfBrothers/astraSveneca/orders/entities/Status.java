package be.bandWidthOfBrothers.astraSveneca.orders.entities;

public enum Status {
    PROCESSING("Wordt verwerkt", 0),
    SHIPPED("Verzonden", 1),
    READY_FOR_PICKUP("Klaar voor afhaling", 2),
    DELIVERED("Afgeleverd", 3),
    PICKED_UP("Afgehaald", 4);

    Status(String name, int statusCode) {
        this.name = name;
        this.statusCode = statusCode;
    }

    private String name;
    private int statusCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
