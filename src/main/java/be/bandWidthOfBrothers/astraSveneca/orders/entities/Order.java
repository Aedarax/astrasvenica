package be.bandWidthOfBrothers.astraSveneca.orders.entities;

import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name="Orders")
public class Order implements Comparable<Order> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @ManyToOne
    private Customer customer;
    @Column
    private LocalDateTime localDateTime;

    public Order() {
        setLocalDateTime(LocalDateTime.now());
    }

    public Order(Customer customer) {
        setLocalDateTime(LocalDateTime.now());
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String formatLocalDateTime() {
        return localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
    }

    @Override
    public int compareTo(Order o) {
        return o.getId() - this.getId();
    }
}
