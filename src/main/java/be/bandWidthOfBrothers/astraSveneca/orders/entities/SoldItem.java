package be.bandWidthOfBrothers.astraSveneca.orders.entities;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="SoldItems")
public class SoldItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @ManyToOne
    @JoinColumn(name="order_id")
    private Order order;
    @ManyToOne
    private Medication medication;
    @ManyToOne
    private Pharmacy pharmacy;
    @Column(name = "Amount")
    private int amount;
    @Column(name = "Price")
    private double price;
    @Column(name = "Status")
    private Status status;
    @Column
    private boolean delivery;
    @Column(name = "Remark")
    private String remark;
    @Column(name = "PickupDate")
    private LocalDate pickupDate;

    public SoldItem() {
    }

    public SoldItem(Order order, Medication medication, Pharmacy pharmacy, int amount, double price, String remark) {
        this.order = order;
        this.medication = medication;
        this.pharmacy = pharmacy;
        this.amount = amount;
        this.price = price;
        this.remark = remark;
        setStatus(Status.PROCESSING);
    }

    public SoldItem(Order order, Medication medication, Pharmacy pharmacy, int amount, double price, String remark, LocalDate pickupDate) {
        this.order = order;
        this.medication = medication;
        this.pharmacy = pharmacy;
        this.amount = amount;
        this.price = price;
        this.remark = remark;
        this.pickupDate = pickupDate;
        setStatus(Status.PROCESSING);
    }

    public int getId() {
        return id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public Pharmacy getPharmacy() {
        return pharmacy;
    }

    public void setPharmacy(Pharmacy pharmacy) {
        this.pharmacy = pharmacy;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
