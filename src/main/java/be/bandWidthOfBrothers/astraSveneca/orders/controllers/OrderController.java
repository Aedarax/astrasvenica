package be.bandWidthOfBrothers.astraSveneca.orders.controllers;

import be.bandWidthOfBrothers.astraSveneca.orders.entities.Order;
import be.bandWidthOfBrothers.astraSveneca.orders.entities.SoldItem;
import be.bandWidthOfBrothers.astraSveneca.orders.repositories.OrderRepo;
import be.bandWidthOfBrothers.astraSveneca.orders.repositories.SoldItemRepo;
import be.bandWidthOfBrothers.astraSveneca.supplies.repository.SupplyRepo;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("orders")
public class OrderController {
    private UserRepo userRepo;
    private OrderRepo orderRepo;
    private SoldItemRepo soldItemRepo;
    private SupplyRepo supplyRepo;

    public OrderController(UserRepo userRepo, OrderRepo orderRepo, SoldItemRepo soldItemRepo, SupplyRepo supplyRepo) {
        this.userRepo = userRepo;
        this.orderRepo = orderRepo;
        this.soldItemRepo = soldItemRepo;
        this.supplyRepo = supplyRepo;
    }

    @GetMapping
    public String showOrders(Model model, Principal principal) {
        Customer customer = userRepo.findCustomer(principal.getName());
        Pharmacy pharmacy = userRepo.findPharmacy(principal.getName());
        Doctor doctor = userRepo.findDoctor(principal.getName());
        List<SoldItem> soldItems = new ArrayList<>();
        Map<Order, List<SoldItem>> map = new TreeMap<>();
        List<Customer> customers = new ArrayList<>();
        List<Order> orders = new ArrayList<>();

        if(checkIfCustomerIsLoggedIn(customer, principal)) {
            orders = orderRepo.findOrdersByCustomerOrderByIdDesc(customer);
            for(Order order : orders) {
                soldItems = soldItemRepo.findSoldItemsByOrder(order);
                map.put(order, soldItems);
            }
        }

        if(checkIfPharmacyIsLoggedIn(pharmacy, principal)) {
            soldItems = soldItemRepo.findSoldItemsByPharmacyOrderByOrderLocalDateTimeDesc(pharmacy);
            for (SoldItem s : soldItems){
                s.setPrice(supplyRepo.findSupplyByMedicationAndPharmacy(s.getMedication(), pharmacy).getPrice());
            }
            customers = soldItems.stream().map(SoldItem::getOrder).distinct()
                    .map(Order::getCustomer).collect(Collectors.toList());
        }

        model.addAttribute("customer", customer);
        model.addAttribute("pharmacy", pharmacy);
        model.addAttribute("doctor", doctor);
        model.addAttribute("soldItems", soldItems);
        model.addAttribute("map", map);
        model.addAttribute("customers", customers);
        model.addAttribute("orders", orders);

        return "orders/OrderView";
    }

    boolean checkIfCustomerIsLoggedIn(Customer customer, Principal principal) {
        return customer != null && customer.getUserName().equals(principal.getName());
    }

    boolean checkIfPharmacyIsLoggedIn(Pharmacy pharmacy, Principal principal) {
        return pharmacy != null && pharmacy.getUserName().equals(principal.getName());
    }
}
