package be.bandWidthOfBrothers.astraSveneca.supplies.entities;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;

import javax.persistence.*;
/**
 * Behlül, Nihad, Robin
 */
@Entity
@Table(name = "Supplies")
public class Supply {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "Price")
    private double price;
    @Column(name = "Stock")
    private int stock;
    @ManyToOne
    private Medication medication;
    @ManyToOne
    private Pharmacy pharmacy;
    @Transient
    private int amount;

    public Supply() {
    }

    public Supply(Medication medication, Pharmacy pharmacy) {
        this.medication = medication;
        this.pharmacy = pharmacy;
    }

    public int getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public Pharmacy getPharmacy() {
        return pharmacy;
    }

    public void setPharmacy(Pharmacy pharmacy) {
        this.pharmacy = pharmacy;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return String.format("Naam apotheek: %s%n" +
                "Prijs: €%f%n" +
                "Stock: %d stuks%n", this.pharmacy.getName(), this.getPrice(), this.getStock());
    }
}
