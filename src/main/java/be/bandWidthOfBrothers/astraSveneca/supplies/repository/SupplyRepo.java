package be.bandWidthOfBrothers.astraSveneca.supplies.repository;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import be.bandWidthOfBrothers.astraSveneca.supplies.entities.Supply;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;
/**
 * Behlül, Nihad, Robin
 */
@Repository
public interface SupplyRepo extends JpaRepository<Supply,Integer> {

    Supply findSupplyByMedicationAndPharmacy(Medication medication, Pharmacy pharmacy);
    Set<Supply> findSuppliesByPharmacy(Pharmacy pharmacy);


}
