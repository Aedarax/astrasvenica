package be.bandWidthOfBrothers.astraSveneca.medication.repository;

import be.bandWidthOfBrothers.astraSveneca.medication.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepo extends JpaRepository<Medication,Integer> {


    List<Medication> findAll();
    Medication findMedicationById(int id);

}