package be.bandWidthOfBrothers.astraSveneca.medication;

import be.bandWidthOfBrothers.astraSveneca.supplies.entities.Supply;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

/**
 * Behlül, Nihad, Robin
 */
@Entity
@Table(name = "Medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "Name")
    @NotBlank
    private String name;
    @Column(name = "Description")
    @NotBlank
    private String description;
    @Column(name = "Image")
    @NotBlank
    private String image;
    @OneToMany(mappedBy = "medication")
    private Set<Supply> supplySet = new HashSet<>();
    @Column(name = "PrescriptionRequired")
    private boolean prescriptionRequired;


    public Medication() {
    }

    public Medication(String name, String description, String image, boolean prescriptionRequired) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.prescriptionRequired = prescriptionRequired;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Set<Supply> getSupplySet() {
        return supplySet;
    }

    public void setSupplySet(Set<Supply> supplySet) {
        this.supplySet = supplySet;
    }

    public boolean isPrescriptionRequired() {
        return prescriptionRequired;
    }

    public void setPrescriptionRequired(boolean prescriptionRequired) {
        this.prescriptionRequired = prescriptionRequired;
    }
}
