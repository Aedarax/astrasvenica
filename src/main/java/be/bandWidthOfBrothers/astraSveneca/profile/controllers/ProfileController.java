package be.bandWidthOfBrothers.astraSveneca.profile.controllers;

import be.bandWidthOfBrothers.astraSveneca.supplies.entities.Supply;
import be.bandWidthOfBrothers.astraSveneca.supplies.repository.SupplyRepo;
import be.bandWidthOfBrothers.astraSveneca.users.pharmacy.Pharmacy;
import be.bandWidthOfBrothers.astraSveneca.users.User;
import be.bandWidthOfBrothers.astraSveneca.users.customer.Customer;
import be.bandWidthOfBrothers.astraSveneca.users.doctor.Doctor;
import be.bandWidthOfBrothers.astraSveneca.users.repositories.UserRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.security.Principal;
import java.util.Set;

@Controller
@RequestMapping("profile")
public class ProfileController {
    private UserRepo userRepo;
    private SupplyRepo supplyRepository;

    public ProfileController(UserRepo userRepo, SupplyRepo supplyRepository) {
        this.userRepo = userRepo;
        this.supplyRepository = supplyRepository;
    }

    @GetMapping()
    public String showItems(Model model, Principal principal) {
        Customer customer = userRepo.findCustomer(principal.getName());
        Pharmacy pharmacy = userRepo.findPharmacy(principal.getName());
        Doctor doctor = userRepo.findDoctor(principal.getName());
        User user = userRepo.findUsersByUserNameEquals(principal.getName()).get(0);
        if(pharmacy != null) {
            Set<Supply> supplies = supplyRepository.findSuppliesByPharmacy(pharmacy);
            model.addAttribute("supplies", supplies);
        }
        model.addAttribute("user", user);
        model.addAttribute("customer", customer);
        model.addAttribute("pharmacy", pharmacy);
        model.addAttribute("doctor", doctor);
        return "profile/ProfileView";

    }
}
