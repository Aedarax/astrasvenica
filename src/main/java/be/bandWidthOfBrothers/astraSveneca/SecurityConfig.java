package be.bandWidthOfBrothers.astraSveneca;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;


@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private DataSource dataSource;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public SecurityConfig(DataSource dataSource, PasswordEncoder passwordEncoder) {
        this.dataSource = dataSource;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder)
                .usersByUsernameQuery("select * from (select coalesce(Pharmacies.UserName, Customers.UserName) as name, coalesce(Pharmacies.passwordbc, Customers.passwordbc) as password, " +
                        " coalesce(Pharmacies.enabled, Customers.enabled) as enable FROM " +
                        " Pharmacies LEFT JOIN Customers ON Pharmacies.Id = Customers.Id " +
                        " UNION " +
                        " SELECT coalesce(Pharmacies.UserName, Customers.UserName) as name, coalesce(Pharmacies.passwordbc, Customers.passwordbc) as password, " +
                        " coalesce(Pharmacies.enabled, Customers.enabled) as enable FROM " +
                        " Pharmacies RIGHT JOIN Customers ON Customers.Id = Pharmacies.Id" +
                        " UNION " +
                        " SELECT coalesce(Doctors.UserName, Customers.UserName), coalesce(Doctors.passwordbc, Customers.passwordbc), coalesce(Doctors.enabled, Customers.enabled) from Doctors left join Customers" +
                        " on Doctors.Id = Customers.Id" +
                        " UNION " +
                        " SELECT coalesce(Doctors.UserName, Customers.UserName), coalesce(Doctors.passwordbc, Customers.passwordbc), coalesce(Doctors.enabled, Customers.enabled) from Doctors right join Customers" +
                        " on Doctors.Id = Customers.Id) lookup where lookup.name = ?")

                .authoritiesByUsernameQuery("select * from (select coalesce(Pharmacies.UserName, Customers.UserName) as name, coalesce(Pharmacies.role, Customers.role) as role " +
                        " FROM " +
                        " Pharmacies LEFT JOIN Customers ON Pharmacies.Id = Customers.Id " +
                        " UNION " +
                        " SELECT coalesce(Pharmacies.UserName, Customers.UserName) as name, coalesce(Pharmacies.role, Customers.role) as role " +
                        " FROM " +
                        " Pharmacies RIGHT JOIN Customers ON Customers.Id = Pharmacies.Id" +
                        " UNION " +
                        " SELECT coalesce(Doctors.UserName, Customers.UserName), coalesce(Doctors.role, Customers.role) from Doctors left join Customers" +
                        " on Doctors.Id = Customers.Id" +
                        " UNION " +
                        " SELECT coalesce(Doctors.UserName, Customers.UserName), coalesce(Doctors.role, Customers.role) from Doctors right join Customers" +
                        " on Doctors.Id = Customers.Id) lookup where lookup.name = ?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .httpBasic()
                .and()
                .authorizeRequests().antMatchers("/register/**","/css/**","/","/login/**","/img/**","/js/**","/cart/**","/productDetail/**").permitAll()
                .antMatchers("/profile/**", "/orders/**", "/prescriptions/**").authenticated()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").permitAll()
                .defaultSuccessUrl("/")
                .and()
                .logout().logoutSuccessUrl("/").permitAll();
    }
}
